<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Inicio</title>
  <style>
    /* Reset de estilos */
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    /* Estilos generales */
    body {
      font-family: Arial, sans-serif;
      background-image: url('Imagenes/fondo.jpg');
      background-size: cover;
      background-position: center;
      background-repeat: no-repeat;
      background-attachment: fixed;
    }

    .container {
      max-width: 1200px;
      margin: 0 auto;
      padding: 0 20px;
    }

    header {
      background-color: rgba(0, 0, 0, 0.5); /* Fondo semi-transparente para mejorar la legibilidad del texto */
      color: #fff;
      padding: 20px 0;
    }

    header h1 {
      margin: 0;
      padding: 0;
      font-size: 24px;
    }

    nav ul {
      list-style: none;
    }

    nav ul li {
      display: inline;
      margin-right: 20px;
    }

    nav ul li a {
      color: #fff;
      text-decoration: none;
    }

    .hero {
      text-align: center;
      padding: 100px 0;
      background-color: rgba(0, 0, 0, 0.5); /* Fondo semi-transparente para mejorar la legibilidad del texto */
      color: #fff;
    }

    .hero h2 {
      margin-bottom: 20px;
    }

    .hero p {
      margin-bottom: 40px;
    }

    .btn {
      display: inline-block;
      padding: 10px 20px;
      background-color: #333;
      color: #fff;
      text-decoration: none;
      border-radius: 5px;
      transition: background-color 0.3s ease;
    }

    .btn:hover {
      background-color: #555;
    }

    footer {
      background-color: rgba(0, 0, 0, 0.5); /* Fondo semi-transparente para mejorar la legibilidad del texto */
      color: #fff;
      text-align: center;
      padding: 20px 0;
    }
  </style>
</head>
<body>
  <header>
    <div class="container">
      <h1>Bienvenido a nuestra Página de Inicio</h1>
      <nav>
        <ul>
          <li><a href="#">Inicio</a></li>
          <li><a href="#">Acerca de</a></li>
          <li><a href="#">Servicios</a></li>
          <li><a href="#">Contacto</a></li>
        </ul>
      </nav>
    </div>
  </header>

  <section class="hero">
    <div class="container">
      <h2>Descubre un Nuevo Mundo de Oportunidades</h2>
      <p>Explora nuestras características y servicios increíbles.</p>
      <a href="#" class="btn">Ver más</a>
    </div>
  </section>

  <footer>
    <div class="container">
      <p>Derechos de autor &copy; 2024 Nuestra Empresa.</p>
    </div>
  </footer>
</body>
</html>
