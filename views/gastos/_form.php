<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Gastos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="gastos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'saldo')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'concepto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idobjetivofinanciero')->textInput() ?>

    <?= $form->field($model, 'idcompra')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
