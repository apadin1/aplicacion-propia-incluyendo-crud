<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ingresos $model */

$this->title = 'Create Ingresos';
$this->params['breadcrumbs'][] = ['label' => 'Ingresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingresos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
