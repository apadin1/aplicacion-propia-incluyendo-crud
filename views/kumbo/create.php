<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Kumbo $model */

$this->title = 'Create Kumbo';
$this->params['breadcrumbs'][] = ['label' => 'Kumbos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kumbo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
