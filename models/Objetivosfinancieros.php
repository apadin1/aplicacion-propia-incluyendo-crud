<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetivosfinancieros".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $objeto
 *
 * @property Precios[] $precios
 */
class Objetivosfinancieros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetivosfinancieros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'objeto'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'objeto' => 'Objeto',
        ];
    }

    /**
     * Gets query for [[Precios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrecios()
    {
        return $this->hasMany(Precios::class, ['idobjetivofinanciero' => 'id']);
    }
}
