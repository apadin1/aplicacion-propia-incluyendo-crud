<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingresos".
 *
 * @property int $id
 * @property int|null $saldo
 * @property string|null $fecha
 * @property string|null $concepto
 * @property string|null $emisor
 * @property int|null $idobjetivofinanciero
 * @property int|null $idventa
 *
 * @property Ventas $idventa0
 */
class Ingresos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['saldo', 'idobjetivofinanciero', 'idventa'], 'integer'],
            [['fecha'], 'safe'],
            [['concepto'], 'string', 'max' => 200],
            [['emisor'], 'string', 'max' => 100],
            [['idventa'], 'unique'],
            [['idventa'], 'exist', 'skipOnError' => true, 'targetClass' => Ventas::class, 'targetAttribute' => ['idventa' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'saldo' => 'Saldo',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'emisor' => 'Emisor',
            'idobjetivofinanciero' => 'Idobjetivofinanciero',
            'idventa' => 'Idventa',
        ];
    }

    /**
     * Gets query for [[Idventa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdventa0()
    {
        return $this->hasOne(Ventas::class, ['id' => 'idventa']);
    }
}
