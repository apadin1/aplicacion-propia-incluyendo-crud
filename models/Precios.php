<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "precios".
 *
 * @property int $id
 * @property int|null $precio
 * @property int|null $idobjetivofinanciero
 *
 * @property Objetivosfinancieros $idobjetivofinanciero0
 */
class Precios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'precios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio', 'idobjetivofinanciero'], 'integer'],
            [['idobjetivofinanciero', 'precio'], 'unique', 'targetAttribute' => ['idobjetivofinanciero', 'precio']],
            [['idobjetivofinanciero'], 'exist', 'skipOnError' => true, 'targetClass' => Objetivosfinancieros::class, 'targetAttribute' => ['idobjetivofinanciero' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'precio' => 'Precio',
            'idobjetivofinanciero' => 'Idobjetivofinanciero',
        ];
    }

    /**
     * Gets query for [[Idobjetivofinanciero0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdobjetivofinanciero0()
    {
        return $this->hasOne(Objetivosfinancieros::class, ['id' => 'idobjetivofinanciero']);
    }
}
